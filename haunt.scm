;;; datashards.org website
;;; Copyright © 2019 Christopher Lemmer Webber <cwebber@dustycloud.org>
;;;
;;; Released under Apache v2; see LICENSE for details.
 
(use-modules (ice-9 match)
             (srfi srfi-1)
             (srfi srfi-11)
             (haunt asset)
             (haunt html)
             (haunt site)
             (haunt page)
             (haunt post)
             (haunt utils)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder rss)
             (haunt builder assets)
             (haunt reader)
             (haunt reader skribe)
             (haunt reader commonmark)
             (web uri))


;;; Utilities
;;; ---------

(define %site-prefix (make-parameter ""))

(define (prefix-url url)
  (string-append (%site-prefix) url))


;;; Templates
;;; ---------

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(prefix-url (string-append "/static/css/" name ".css"))))))

(define (base-image image alt)
  `(img (@ (src ,(prefix-url (string-append "/static/images/" image)))
           (alt ,alt))))

(define* (base-tmpl site body
                    #:key title)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (title ,(if title
                 (string-append title " -- " (site-title site))
                 (site-title site)))
     ;; css
     ,(stylesheet "main"))
    (body
     (div (@ (class "main-wrapper"))
          (header (@ (id "site-header"))
                  ;; Site logo
                  (div (@ (class "header-logo-wrapper"))
                       (a (@ (href ,(prefix-url "/")))
                          ,(base-image "Datashards-logo.png"
                                       "Datashards logo"))))
          (div (@ (class "site-main-content"))
               ,body))
     ;; TODO: Link to source.
     (div (@ (class "footer"))
          (a (@ (href "https://gitlab.com/datashards/datashards-website"))
             "Site contents")
          " released under the "
          (a (@ (href "https://www.apache.org/licenses/LICENSE-2.0"))
             "Apache License version 2.0")
          ".  Datashards logo by mray, released into public domain under "
          (a (@ (href "https://creativecommons.org/publicdomain/zero/1.0/"))
             "CC0 1.0")
          ".  Powered by "
          (a (@ (href "http://haunt.dthompson.us/"))
             "Haunt")
          "."))))

(define* (post-template post #:key post-link)
  (define enclosures
    (reverse (post-ref-all post 'enclosure)))
  `(div (@ (class "content-box blogpost"))
        (h1 (@ (class "title"))
            ,(if post-link
                 `(a (@ (href ,post-link))
                     ,(post-ref post 'title))
                 (post-ref post 'title)))
        (div (@ (class "post-about"))
             (span (@ (class "by-line"))
                   ,(post-ref post 'author))
             " -- " ,(date->string* (post-date post)))
        (div (@ (class "post-body"))
             ,(post-sxml post))))

(define (post-uri site post)
  (prefix-url
   (string-append "/news/" (site-post-slug site post) ".html")))

(define (collection-template site title posts prefix)
  ;; In our case, we ignore the prefix argument because
  ;; the filename generated and the pathname might not be the same.
  ;; So we use (prefix-url) instead.
  `((div (@ (class "posts-header"))
         (h3 "recent posts"))
    (div (@ (class "post-list"))
         ,@(map
            (lambda (post)
              (post-template post #:post-link (post-uri site post)))
            posts))))

(define datashards-haunt-theme
  (theme #:name "Datashards"
         #:layout
         (lambda (site title body)
           (base-tmpl
            site body
            #:title title))
         #:post-template post-template
         #:collection-template collection-template))

;; Borrowed from davexunit's blog
(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define* (element-join items #:key [delim " "])
  (let lp ((items items)
           (count 0))
    (match items
      ('() '())
      ((item rest ...)
       (cond
        ((zero? count)
         (cons item (lp rest (1+ count))))
        (else
         (cons delim (cons item (lp rest (1+ count))))))))))

(define (post-preview post site)
  `(li (a (@ (href ,(post-uri site post)))
          ,(post-ref post 'title))
       (div (@ (class "news-feed-content"))
            (div (@ (class "news-feed-item-date"))
                 ,(date->string* (post-date post)))
            ,(first-paragraph post)
            (div (@ (class "consume-more-buttons"))
                 (a (@ (href ,(post-uri site post)))
                         "[Read more ==>]")))))


;;; Pages
(define (index-content site posts)
  `(div
    ;; Main intro
    (div (@ (class "content-box bigger-text")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (p "Datashards brings secure storage foundations to the modern web. "
            "It allows users and application developers to reason about "
            "secure, private data online or offline." )
         (p "Applications can be built on top of Datashards that provide "
            "features such as shared storage, always-available data, "
            "private communication and more." )
         (h2 (@ (class "title")
                (id "subscribe"))
             "Tutorials & Specifications")
         (p "Datashards is still in early development and is subject to "
            "change as it becomes more mature. As of now there are no official"
            ", formal specifications. Nonethless we provide the following "
            "writeups that explain the rationale and some implementation "
            "details." )
         (ul (li (a (@ (href "https://github.com/WebOfTrustInfo/rwot9-prague/blob/master/topics-and-advance-readings/datashards-rationale.md"))
                    "Datashards Rationale"))
             (li (a (@ (href "https://gitlab.com/dustyweb/magenc/blob/master/magenc/scribblings/intro.org"))
                    "Magenc introduction (former version of IDSC)"))
             (li (a (@ (href "https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org"))
                    "Crystal introduction (former version of MDSC)")))
         (h2 (@ (class "title")
                (id "subscribe"))
             "Implementations")
         (p "As Datashards is still in the early specification phase, all the"
            "implementation should be considered ongoing works in the alpha"
            "stage.  However, we do have implementations!")
         (ul (li (a (@ (href "https://gitlab.com/spritely/racket-datashards"))
                    "racket-datashards"))
             (li (a (@ (href "https://gitlab.com/emacsen/pydatashards"))
                    "PyDatashards"))))))

(define (index-page site posts)
  (make-page
   "index.html"
   (base-tmpl site
              (index-content site posts))
   sxml->html))


;;; Site

(define max-entries 1024)

(site #:title "Datashards"
      #:default-metadata
      '((author . "Datashards Team"))
      #:readers (list skribe-reader commonmark-reader)
      #:builders (list (blog #:prefix "/news"
                             #:theme datashards-haunt-theme)
                       index-page
                       (rss-feed #:blog-prefix "/news"
                                 #:max-entries max-entries
                                 #:file-name "rss-feed.rss")
                       (static-directory "static" "static")))
